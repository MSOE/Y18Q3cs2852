package wk2;

import java.util.Collection;

public class Tester {
    private static final String[] EMPTY_STRINGS = {};
    private static final String[] STRINGS1 = {"this"};
    private static final String[] STRINGS2 = {"this", "will"};
    private static final String[] STRINGS3 = {"this", "will", "be"};
    private static final String[] STRINGS4 = {"this", "will", "be", "interesting"};
    private static final String[] STRINGS5 = {"this", null, "will", "be", "interesting"};

    private static final int[] EMPTY_INTS = {};
    private static final int[] INTS1 = {-1};
    private static final int[] INTS2 = {-1, 2};
    private static final int[] INTS3 = {-1, 2, 8};
    private static final int[] INTS4 = {-1, 2, 8, Integer.MAX_VALUE};

    public static void main(String[] ignored) {
        String description = "ArrayList constructor collection of Strings of size: ";
        test(description + 0, stringConstructorTest(EMPTY_STRINGS));
        test(description + 1, stringConstructorTest(STRINGS1));
        test(description + 2, stringConstructorTest(STRINGS2));
        test(description + 3, stringConstructorTest(STRINGS3));
        test(description + 4, stringConstructorTest(STRINGS4));
        test(description + 5, stringConstructorTest(STRINGS5));

        description = "ArrayList constructor collection of Integers of size: ";
        test(description + 0, intConstructorTest(EMPTY_INTS));
        test(description + 1, intConstructorTest(INTS1));
        test(description + 2, intConstructorTest(INTS2));
        test(description + 3, intConstructorTest(INTS3));
        test(description + 4, intConstructorTest(INTS4));

        description = "Find null in ArrayList";
        test(description, testForNull(STRINGS5));
    }

    private static void test(String description, boolean passed) {
        if(!passed) {
            System.out.println("Test failed: " + description);
        }
    }

    private static boolean stringConstructorTest(String[] strings) {
        Collection<String> expected = arrayToCollection(strings, new java.util.ArrayList<>());
        Collection<String> actual = arrayToCollection(strings, new wk1.ArrayList<>());
        return actual.size()==expected.size();
    }

    private static boolean intConstructorTest(int[] numbers) {
        Integer[] integers = new Integer[numbers.length];
        for(int i = 0; i<numbers.length; ++i) {
            integers[i] = numbers[i];
        }
        Collection<Integer> expected = arrayToCollection(integers, new java.util.ArrayList<>());
        Collection<Integer> actual = arrayToCollection(integers, new wk1.ArrayList<>());
        return actual.size()==expected.size();
    }

    private static <E> Collection<E> arrayToCollection(E[] elements, Collection<E> collection) {
        for(E element : elements) {
            collection.add(element);
        }
        return collection;
    }

    private static boolean testForNull(String[] strings) {
        Collection<String> list = arrayToCollection(strings, new wk1.ArrayList<>());
        return list.contains(null);
    }

}
