package wk8;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class HashTable<E> implements Set<E> {
    private List<E>[] buckets;

    private static final int DEFAULT_SIZE = 19;

    public HashTable() {
        this(DEFAULT_SIZE);
    }

    public HashTable(int size) {
        buckets = new LinkedList[size];
    }

    @Override
    public int size() {
        return Arrays.stream(buckets)
                .filter(b -> b!=null)
                .mapToInt(b -> b.size())
                .sum();
//        int size = 0;
//        for(List<E> bucket : buckets) {
//            size += bucket==null ? 0 : bucket.size();
//        }
//        return size;
    }

    @Override
    public boolean isEmpty() {
        return Arrays.stream(buckets)
                .filter(b -> b!=null)
                .count()==0;
    }

    @Override
    public boolean contains(Object target) {
        int index = getIndex(target);
        return buckets[index]!=null && buckets[index].contains(target);
    }

    @Override
    public Iterator<E> iterator() {
        return Arrays.stream(buckets)
                .filter(b -> b!=null)
                .flatMap(List::stream)
                .iterator();
    }

    @Override
    public Object[] toArray() {
        return Arrays.stream(buckets)
                .filter(b -> b!=null)
                .flatMap(List::stream)
                .collect(Collectors.toList())
                .toArray();
    }

    @Override
    public boolean add(E element) {
        boolean isChanged = false;
        if(!contains(element)) {
            int index = getIndex(element);
            if(buckets[index]==null) {
                buckets[index] = new LinkedList<>();
            }
            isChanged = buckets[index].add(element);
        }
        return isChanged;
    }

    private int getIndex(Object element) {
        return element==null ? 0 : Math.abs(element.hashCode()%buckets.length);
    }

    @Override
    public boolean remove(Object target) {
        int index = getIndex(target);
        return buckets[index]!=null && buckets[index].remove(target);
    }

    @Override
    public void clear() {
        buckets = new LinkedList[DEFAULT_SIZE];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }
}
