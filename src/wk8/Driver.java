package wk8;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Map<String, Integer> words = new HashMap<>();
        try (Scanner in = new Scanner((new URL("http://msoe.us/taylor/cs2852/Lab8")).openStream())) {
            while(in.hasNext()) {
                updateCount(words, in.next());
            }
            System.out.println("\"the\" is found " + words.get("the") + " times.");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void updateCount(Map<String, Integer> words, String word) {
        Integer count = words.get(word);
        if(count==null) {
            count = 0;
        }
        words.put(word, ++count);
    }
}
