package wk4;

public interface PureStack<E> {
    E peek();
    void push(E element);
    E pop();
    boolean isEmpty();
}
