package wk4;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

public class Stack<E> implements PureStack<E> {
    private List<E> stack;

    public Stack() {
        stack = new ArrayList<>();
    }

    @Override
    public E peek() {
        if(isEmpty()) {
            throw new EmptyStackException();
        }
        return stack.get(stack.size()-1);
    }

    @Override
    public void push(E element) {
        stack.add(element);
    }

    @Override
    public E pop() {
        if(isEmpty()) {
            throw new EmptyStackException();
        }
        return stack.remove(stack.size()-1);
    }

    @Override
    public boolean isEmpty() {
        return stack.isEmpty();
    }
}
