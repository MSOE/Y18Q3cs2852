package wk6;

import java.math.BigInteger;

public class Driver {
    public static void main(String[] args) {
        BinarySearchTree<BigInteger> treeOfBigNumbers = new BinarySearchTree<>();
        treeOfBigNumbers.add(new BigInteger("999999999999999"));
        treeOfBigNumbers.add(new BigInteger("30000000"));
        treeOfBigNumbers.add(new BigInteger("3"));
        treeOfBigNumbers.printTreeInOrder();
        //System.out.println(treeOfBigNumbers.size());
        //System.out.println(treeOfBigNumbers.isEmpty());

    }
}
