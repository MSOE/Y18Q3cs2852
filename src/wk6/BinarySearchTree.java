package wk6;

public class BinarySearchTree<E extends Comparable<E>> {
    private static class Node<E> {
        E value;
        Node<E> lKid;
        Node<E> rKid;
        Node(E value) {
            this(value, null, null);
        }

        Node(E value, Node<E> left, Node<E> right) {
            this.value = value;
            lKid = left;
            rKid = right;
        }
    }

    private Node<E> root;

    public boolean isEmpty() {
        return root==null;
    }

    public int size() {
        return size(root);
    }

    private int size(Node<E> subroot) {
        int size = 0;
        if(subroot!=null) {
            size = 1 + size(subroot.lKid) + size(subroot.rKid);
        }
        return size;
    }

    public int height() {
        return height(root);
    }

    private int height(Node<E> subroot) {
        int height = 0;
        if(subroot!=null) {
            height = 1 + Math.max(height(subroot.lKid), height(subroot.rKid));
        }
        return height;
    }

    public E maxUnsorted() {
        return root==null ? null : maxUnsorted(root);
    }

    private E maxUnsorted(Node<E> subroot) {
        E answerLeft = subroot.lKid!=null ? maxUnsorted(subroot.lKid) : subroot.value;
        E answerRight = subroot.rKid!=null ? maxUnsorted(subroot.rKid) : subroot.value;
        E answer = subroot.value;
        if(answerLeft.compareTo(answer)>0) {
            answer = answerLeft;
        }
        if(answerRight.compareTo(answer)>0) {
            answer = answerRight;
        }
        return answer;
    }

    public void printTreePreOrder() {
        printTreePreOrder(root);
    }

    private void printTreePreOrder(Node<E> subroot) {
        if(subroot!=null) {
            System.out.println(subroot.value);
            printTreePreOrder(subroot.lKid);
            printTreePreOrder(subroot.rKid);
        }
    }

    public void printTreePostOrder() {
        printTreePostOrder(root);
    }

    private void printTreePostOrder(Node<E> subroot) {
        if(subroot!=null) {
            printTreePostOrder(subroot.lKid);
            printTreePostOrder(subroot.rKid);
            System.out.println(subroot.value);
        }
    }

    public void printTreeInOrder() {
        printTreeInOrder(root);
    }

    private void printTreeInOrder(Node<E> subroot) {
        if(subroot!=null) {
            printTreeInOrder(subroot.lKid);
            System.out.println(subroot.value);
            printTreeInOrder(subroot.rKid);
        }
    }

    public E max() {
        return root==null ? null : max(root);
    }

    private E max(Node<E> subroot) {
        E max = subroot.value;
        if(subroot.rKid!=null) {
            max = max(subroot.rKid);
        }
        return max;
    }

    public boolean add(E value) {
        if(value==null) {
            throw new NullPointerException("Nulls not welcome here");
        }
        boolean isChanged;
        if(root==null) {
            root = new Node<>(value);
            isChanged = true;
        } else {
            isChanged = add(root, value);
        }
        return isChanged;
    }

    private boolean add(Node<E> subroot, E value) {
        boolean isChanged = false;
        int comparison = value.compareTo(subroot.value);
        if(comparison>0) {
            if(subroot.rKid==null) {
                subroot.rKid = new Node<>(value);
                isChanged = true;
            } else {
                isChanged = add(subroot.rKid, value);
            }
        } else if(comparison<0) {
            if(subroot.lKid==null) {
                subroot.lKid = new Node<>(value);
                isChanged = true;
            } else {
                isChanged = add(subroot.lKid, value);
            }
        }
        return isChanged;
    }
}
