package wk9;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class BinarySearchTree<E extends Comparable<E>> {
    public static void main(String[] args) {
        BinarySearchTree<String> tree = new BinarySearchTree<>();
        tree.add("dog");
        tree.add("flop");
        tree.add("sun");
        tree.add("son");
        tree.add("chalk");
        tree.add("projector");
        tree.add("marker");
        tree.add("hinge");
        tree.printPreOrder();
        System.out.println(tree.drawTree());
    }
    private static class Node<E> {
        E value;
        Node<E> parent;
        Node<E> lKid;
        Node<E> rKid;
        Node(E value) {
            this(value, null, null, null);
        }

        Node(E value, Node<E> parent) {
            this(value, parent, null, null);
        }

        Node(E value, Node<E> parent, Node<E> left, Node<E> right) {
            this.value = value;
            this.parent = parent;
            lKid = left;
            rKid = right;
        }
    }

    private Node<E> root;

    private Node<E> rightRotate(Node<E> subroot) {
        Node<E> A = subroot;
        Node<E> B = A.lKid;
        Node<E> x = B.rKid;
        Node<E> y = A.parent;
        A.lKid = x;
        B.parent = y;
        B.rKid = A;
        A.parent = B;
        if(y!=null) {
            if(A==y.rKid) {
                y.rKid = B;
            } else {
                y.lKid = B;
            }
        }
        if(x!=null) {
            x.parent = A;
        }
        return B;
    }

    public boolean isEmpty() {
        return root==null;
    }

    public int size() {
        return size(root);
    }

    private int size(Node<E> subroot) {
        int size = 0;
        if(subroot!=null) {
            size = 1 + size(subroot.lKid) + size(subroot.rKid);
        }
        return size;
    }

    private void preOrder(BiConsumer<E, Integer> consumer) {
        preOrder(root, 1, consumer);
    }

    private void preOrder(Node<E> subroot, int depth, BiConsumer<E, Integer> consumer) {
        if(subroot!=null) {
            consumer.accept(subroot.value, depth);
            preOrder(subroot.lKid, depth+1, consumer);
            preOrder(subroot.rKid, depth+1, consumer);
        }
    }

    public void printPreOrder() {
        preOrder((value, depth) -> System.out.println(value + " at depth " + depth));
    }

    public String drawTree() {
        final StringBuilder builder = new StringBuilder();
        preOrder((value, depth) -> {
            for(int i = 1; i<depth; ++i) {
                builder.append(" ");
            }
            builder.append(value).append("\n");
        });
        return builder.toString();
    }

    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[");
        inOrder(value -> builder.append(value).append(", "));
        if(builder.length()>2) {
            builder.setLength(builder.length()-2);
        }
        builder.append("]");
        return builder.toString();
    }

    private void inOrder(Consumer<E> consumer) {
        inOrder(root, consumer);
    }

    private void inOrder(Node<E> subroot, Consumer<E> consumer) {
        if(subroot!=null) {
            inOrder(subroot.lKid, consumer);
            consumer.accept(subroot.value);
            inOrder(subroot.rKid, consumer);
        }
    }

    public boolean add(E value) {
        if(value==null) {
            throw new NullPointerException("Nulls not welcome here");
        }
        boolean isChanged;
        if(root==null) {
            root = new Node<>(value);
            isChanged = true;
        } else {
            isChanged = add(root, value);
        }
        return isChanged;
    }

    private boolean add(Node<E> subroot, E value) {
        boolean isChanged = false;
        int comparison = value.compareTo(subroot.value);
        if(comparison>0) {
            if(subroot.rKid==null) {
                subroot.rKid = new Node<>(value, subroot);
                isChanged = true;
            } else {
                isChanged = add(subroot.rKid, value);
            }
        } else if(comparison<0) {
            if(subroot.lKid==null) {
                subroot.lKid = new Node<>(value, subroot);
                isChanged = true;
            } else {
                isChanged = add(subroot.lKid, value);
            }
        }
        return isChanged;
    }
}
