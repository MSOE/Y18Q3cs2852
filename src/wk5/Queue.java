package wk5;

import java.util.LinkedList;

public class Queue<E> implements PureQueue<E> {
    private LinkedList<E> queue;

    @Override
    public boolean offer(E element) {
        if(element==null) {
            throw new NullPointerException("Queue does not support null elements");
        }
        return queue.add(element);
    }

    @Override
    public E poll() {
        return queue.isEmpty() ? null : queue.remove(0);
    }

    @Override
    public E peek() {
        return queue.isEmpty() ? null : queue.get(0);
    }
}
