package wk5;

public interface PureQueue<E> {
    boolean offer(E element);
    E poll();
    E peek();
    default boolean isEmpty() {
        return null==peek();
    }
}
