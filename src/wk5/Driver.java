package wk5;

public class Driver {
    public static void main(String[] args) {
        printInts();
        printIntsRecursive(20);
        System.out.println(sumRecursively(20));
    }

    public boolean search(String[] words, String target) {
        return search(words, target, 0, words.length);
    }

    private boolean search(String[] words, String target, int start, int end) {
        int middleIndex = (start+end)/2;
        boolean found;
        int comparison = target.compareTo(words[middleIndex]);
        if(start==end) {
            found = false;
        } else if(comparison==0) {
            found = true;
        } else if(comparison<0) {
            found = search(words, target, start, middleIndex);
        } else {
            found = search(words, target, middleIndex+1, end);
        }
        return found;
    }

    private static int sumRecursively(int i) {
        int answer = 0;
        if(i>0) {
            answer = i + sumRecursively(i - 1);
        }
        return answer;
    }


    private static void printIntsRecursive(int i) {
        System.out.println(i--);
        if(i>0) {
            printIntsRecursive(i);
        }
        System.out.println("something else");
    }

    private static void printInts() {
        for(int i=20; i>0; --i) {
            System.out.println(i);
        }
    }
}
