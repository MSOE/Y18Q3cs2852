package wk5;

public class CircularQueue<E> implements PureQueue<E> {
    private E[] data;
    private int head;
    private int size;

    public CircularQueue(int capacity) {
        data = (E[])new Object[capacity];
    }

    @Override
    public boolean offer(E element) {
        if(element==null) {
            throw new NullPointerException("Queue does not support null elements");
        }
        boolean changed = false;
        if(size<data.length) {
            data[(head+size)%data.length] = element;
            ++size;
            changed = true;
        }
        return changed;
    }

    @Override
    public E poll() {
        // Implement as homework
        return null;
    }

    @Override
    public E peek() {
        // Implement as homework
        return null;
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }
}
