package wk1;

import wk2.LinkedList;

import java.util.Collection;
import java.util.Iterator;

public class Driver {
    public static void main(String[] args) {
        Collection<String> words = new ArrayList<>();
        words.add("bird");
        words.add("can");
        words.add("bad");

        Iterator<String> itr = words.iterator();
        while(itr.hasNext()) {
            String word = itr.next();
            System.out.println(word);
        }

        for(String word : words) {
            System.out.println(word);
        }
    }
}